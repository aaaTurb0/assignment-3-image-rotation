#include "structs.h"
#include <stdio.h>
#include <stdlib.h>

#define BF_T 19778
#define BI_S 40
#define BI_P 1
#define MEMORY_ERROR 81;
#define HEADER_ERROR 81;

struct bmp_header defaultHeader(void) {
    struct bmp_header header = {0};
    header.bfType = BF_T;
    header.biSize = BI_S;
    header.biPlanes = BI_P;
    return header;
}

struct image initImage(struct bmp_header header) {
    return (struct image) {
            .data = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight),
            .width = header.biWidth,
            .height = header.biHeight
    };
}

int from_bmp(FILE *input, struct image *const read) {
    if (!input || !read)
        return READING_ERROR;

    struct bmp_header header;

    if (!fread(&header, sizeof(struct bmp_header), 1, input))
        return READING_ERROR;

    *read = initImage(header);

    if (!read->data)
        return MEMORY_ERROR;

    uint32_t padding = (4 - (read->width * 3) % 4) % 4;

    for (size_t i = 0; i < header.biHeight; i++) {
        if (!fread(read->data + i * header.biWidth,
                   sizeof(struct pixel) * read->width, 1, input))
            return READING_ERROR;
        fseek(input, padding, SEEK_CUR);
    }
    fclose(input);
    return 0;
}

void buildHeader (struct bmp_header* header, uint32_t width, uint32_t height, uint64_t padding) {
    header->biBitCount = sizeof(struct pixel) * 8;
    header->bOffBits = sizeof(struct bmp_header);
    header->bfileSize = header->bOffBits + (width * 3 + padding) * height;
    header->biWidth = width;
    header->biHeight = height;
}

int to_bmp(FILE *output, struct image *const src) {
    uint32_t padding = (4 - (src->width * 3) % 4) % 4;

    struct bmp_header header = defaultHeader();
    uint32_t width = src->width;
    uint32_t height = src->height;
    buildHeader(&header, width, height, padding);

    char zeroes[] = {0, 0, 0};

    if (!fwrite(&header, sizeof(struct bmp_header), 1, output))
        return HEADER_ERROR;

    for (size_t i = 0; i < header.biHeight; i++) {
        if (!fwrite(src->data + i * src->width, (src->width * 3), 1, output))
            return WRITING_ERROR;
        fwrite(zeroes, padding, 1, output);
    }
    fclose(output);

    return 0;
}
