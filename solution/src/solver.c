#include "structs.h"
#include <stdlib.h>

struct image rotate(struct image const src, int angle) {
    struct image res = {0};
    if (angle == 0) {
        res.width = src.width;
        res.height = src.height;
        res.data = malloc(res.width * sizeof(struct pixel) * res.height);
        for (size_t i = 0; i < src.width * src.height; i++) {
            res.data[i] = src.data[i];
        }
    } else if (angle == 180 || angle == -180) {
        res.width = src.width;
        res.height = src.height;
        res.data = malloc(res.width * sizeof(struct pixel) * res.height);
        for (size_t i = 0; i < res.height; i++) {
            for (size_t j = 0; j < res.width; j++) {
                res.data[src.height * src.width - i * src.width - j - 1] = src.data[i * src.width + j];
            }
        }
    } else if (angle == 270 || angle == -90) {
        res.width = src.height;
        res.height = src.width;
        res.data = malloc(res.width * sizeof(struct pixel) * res.height);
        for (size_t i = 0; i < res.width; i++) {
            for (size_t j = 0; j < res.height; j++) {
                res.data[(j + 1) * res.width - i - 1] = src.data[i * src.width + j];
            }
        }
    } else if (angle == 90 || angle == -270) {
        res.width = src.height;
        res.height = src.width;
        res.data = malloc(res.width * sizeof(struct pixel) * res.height);
        for (size_t i = 0; i < res.height; i++) {
            for (size_t j = 0; j < res.width; j++) {
                res.data[i * res.width + j] = src.data[(j + 1) * src.width - i - 1];
            }
        }
    }
    return res;
}
