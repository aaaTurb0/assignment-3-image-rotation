#include "structs.h"
#include <stdio.h>

int from_bmp(FILE *input, struct image *const read);

int to_bmp(FILE *output, struct image *const src);
