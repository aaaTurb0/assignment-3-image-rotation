#include  <stdint.h>

#define BAD_INPUT 50;
#define BAD_FILE 60;
#define BAD_IMAGE 70;
#define READING_ERROR 80;
#define ROTATING_ERROR 90;
#define BAD_ANGLE 100;
#define WRITING_ERROR 110;

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

struct __attribute__((packed)) pixel {uint8_t r, g, b;};

struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel* data;};
