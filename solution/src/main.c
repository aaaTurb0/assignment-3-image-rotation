#include "bmpWorker.h"
#include "solver.h"
#include <stdio.h>
#include <stdlib.h>



int validateArgc(int argc) {
    if (argc != 4) {
        fprintf(stderr, "Use parameters this way: <BMP_FILE_NAME1> <BMP_FILE_NAME2> <ANGLE>\n");
        return BAD_INPUT;
    }
    return 0;
}

int validateFile(FILE* file) {
    if (!file) {
        fprintf(stderr, "Error while opening 1st file\n");
        return BAD_FILE;
    }
    return 0;
}

int validateImage(struct image* img) {
    if (!img) {
        free(img);
        fprintf(stderr, "Error while allocating memory\n");
        return BAD_IMAGE;
    }
    return 0;
}

void freeSource(struct image* source) {
    free(source->data);
    free(source);
}

void deinitMain(struct image* source, struct image ready_image) {
    freeSource(source);
    free(ready_image.data);
}

int validateAngle(int angle) {
    if (angle % 90 != 0) {
        return BAD_ANGLE;
    }
    return 0;
}

int main(int argc, char **argv) {

    int argcValid = validateArgc(argc);
    if (argcValid != 0) {
        return argcValid;
    }

    FILE *in = fopen(argv[1], "rb");
    int fileValid = validateFile(in);
    if (fileValid != 0) {
        return fileValid;
    }

    FILE *out = fopen(argv[2], "wb");
    fileValid = validateFile(out);
    if (fileValid != 0) {
        return fileValid;
    }

    struct image* source = malloc(sizeof(struct image));
    int imgValid = validateImage(source);
    if (imgValid != 0) {
        return imgValid;
    }

    int read = from_bmp(in, source);
    if (read != 0) {
        freeSource(source);
        fprintf(stderr, "Error while reading\n");
        return read;
    }

    char* angleStr = argv[3];
    int angle = atoi(angleStr);
    int angleValid = validateAngle(angle);
    if (angleValid != 0) {
        fprintf(stderr, "Wrong angle!\n");
        return angleValid;
    }

    struct image ready_image = rotate(*source, angle);
    if (!ready_image.data) {
        freeSource(source);
        fprintf(stderr, "Error while transforming\n");
        return ROTATING_ERROR;
    }

    int write = to_bmp(out, &ready_image);
    if (write != 0) {
        deinitMain(source, ready_image);
        fprintf(stderr, "Error while writing\n");
        return WRITING_ERROR;
    }

    deinitMain(source, ready_image);
    return 0;
}
